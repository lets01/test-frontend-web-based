<!DOCTYPE html>	<html>	
<head>	

	<meta charset="utf-8">	    
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	   

	<title>PersonCAD</title>	    
	<meta name="description" content="">	    
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo BASEURL; ?>css/bootstrap.min.css">

	<link rel="stylesheet" href="<?php echo BASEURL; ?>/css/style.css">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css">	
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

</head>	
<body onLoad="init()">	

    <header class="main-header">

       <div id="logo" >
            <a href="<?php echo BASEURL; ?>index.php" class="navbar-brand" >
                <img src="<?php echo BASEURL; ?>img/LogoBranca.png" style="width:150px; " />
            </a>
        </div>

        <a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="border: none">
            <span id="icone_toggle" style="color: white;" aria-hidden="true" class="glyphicon glyphicon-align-left"></span>&nbsp;
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
          
        </nav>
    </header>

    <anside>     
        <div class="row" id="wrapper">
            <div class="col-md-2 col-sm-4 sidebar1" id="sidebar-wrapper">
                <div class="left-navigation">
                    <ul class="sidebar-nav list" >                        
                        <li>
                            <a href="#submenu1" data-toggle="collapse" >Gerenciar Pessoas <span class="caret"></span></a>
                            <ul class="collapse list-unstyled" id="submenu1">                  
                                <li><a href="<?php echo BASEURL; ?>pessoas">Listar Pessoas</a></li>
                                <li><a href="<?php echo BASEURL; ?>pessoas/add.php">Adicionar nova Pessoa</a></li>
                            </ul>
                        </li>
                        <li> 
	                        <a href="#submenu2" data-toggle="collapse" >Gráficos <span class="caret"></span></a>
	                        <ul  class="collapse list-unstyled" id="submenu2" >
	                            <li><a href="<?php echo BASEURL; ?>pessoas/grafico1.php">Grafico de idades</a></li>
	                            <li><a href="<?php echo BASEURL; ?>pessoas/grafico2.php">Grafico de genero</a></li>
	                        </ul>
	                    </li>   
                    </ul>
                </div>
            </div>
            <div class="col-md-10 col-sm-8 main-content" id="page-content-wrapper">
                 <main class="container-fluid">
       