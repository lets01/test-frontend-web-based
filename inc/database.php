<?php		
	mysqli_report(MYSQLI_REPORT_STRICT);		
	function open_database() {		
		try {			
			$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);			
			return $conn;		
		} 
		catch (Exception $e) {			
			echo $e->getMessage();			
			return null;		
		}	
	}		
	function close_database($conn) {		
		try {			
			mysqli_close($conn);		
		} catch (Exception $e) {			
			echo $e->getMessage();		
		}	
	}

	/*  Pesquisa um Registro pelo ID em uma Tabela	*/	
	function find( $table = null, $id = null ) {	  		
		$database = open_database();		
		$found = null;			
		try {		  
			if ($id) {		   
				$sql = "SELECT * FROM " . $table . " WHERE id = " . $id;		    
				$result = $database->query($sql);		    		    
				if ($result->num_rows > 0) {		      
				$found = $result->fetch_assoc();		    
			}		    		  
			} else {		    		    
				$sql = "SELECT * FROM " . $table;		    
				$result = $database->query($sql);		    		    
				if ($result->num_rows > 0) {		      
					$found = $result->fetch_all(MYSQLI_ASSOC);	        	        
					/* Metodo alternativo	        
					$found = array();		        
					while ($row = $result->fetch_assoc()) {	          
					array_push($found, $row);	      
					} */		    
				}		  
			}		
		}catch (Exception $e) {		 
			$_SESSION['message'] = $e->GetMessage();		  
			$_SESSION['type'] = 'danger';	  
		}				
		close_database($database);		
		return $found;	
	}


	/**	 *  Pesquisa Todos os Registros de uma Tabela	 */	

	function find_all( $table ) {	  
		return find($table);	}

	/**	*  Insere um registro no BD	*/	
	function save($table = null, $data = null) {		  
		$database = open_database();		  
		$columns = null;	 
		$values = null;		  
		//print_r($data);		  
		foreach ($data as $key => $value) {	    
			$columns .= trim($key, "'") . ",";	    
			$values .= "'$value',";	  
		}		 
		// remove a ultima virgula	  
		$columns = rtrim($columns, ',');	  
		$values = rtrim($values, ',');	  	  
		$sql = "INSERT INTO " . $table . "($columns)" . " VALUES " . "($values);";		  
		try {	    
			$database->query($sql);		    
			$_SESSION['message'] = 'Registro cadastrado com sucesso.';	    
			$_SESSION['type'] = 'success';	  	  
		} catch (Exception $e) { 	  	    
			$_SESSION['message'] = 'Nao foi possivel realizar a operacao.';	    
			$_SESSION['type'] = 'danger';	  
		} 		  
		close_database($database);	
	}
	/*  Remove uma linha de uma tabela pelo ID do registro	 */	
	function remove( $table = null, $id = null ) {		  
		$database = open_database();			  
		try {	    
			if ($id) {		      
				$sql = "DELETE FROM " . $table . " WHERE id = " . $id;	      
				$result = $database->query($sql);		      
				if ($result = $database->query($sql)) {   		        
					$_SESSION['message'] = "Registro Removido com Sucesso.";	       
					$_SESSION['type'] = 'success';	      
				}	    
			}	  
		}catch (Exception $e) { 		    
		 	$_SESSION['message'] = $e->GetMessage();	    
		 	$_SESSION['type'] = 'danger';	  
		}		  
		close_database($database);	
	}

	/*  Atualiza um registro em uma tabela, por ID	 */	
	function update($table = null, $id = 0, $data = null) {		  
		$database = open_database();		  
		$items = null;		  
		foreach ($data as $key => $value) {	    
		$items .= trim($key, "'") . "='$value',";	  
		}		  
		// remove a ultima virgula	  
		$items = rtrim($items, ',');		  
		$sql  = "UPDATE " . $table;	  
		$sql .= " SET $items";	  
		$sql .= " WHERE id=" . $id . ";";		  
		try {	    $database->query($sql);		    
			$_SESSION['message'] = 'Registro atualizado com sucesso.';	    
			$_SESSION['type'] = 'success';		  
		} catch (Exception $e) { 		    
			$_SESSION['message'] = 'Nao foi possivel realizar a operacao.';	    
			$_SESSION['type'] = 'danger';	  
		} 		  
		close_database($database);	
	}
	/*  Retorna a faixa de idades e a quantidade de pessoas relacionada a cada idade  */
	function idades(){

	      $database = open_database();
	      
	  	 //consulta que retorna a faixa de idades e a quantidade de pessoas relacionada a cada uma //
	      $database->real_query(" SELECT case when truncate(datediff(curdate(), c.birthdate) / 365.25, 0) <= 9 then  
										'ate 9'  
									when truncate(datediff(curdate(), c.birthdate) / 365.25, 0)  between 10 and 19 then  
										'de 10 a 19'  
									when  truncate(datediff(curdate(), c.birthdate) / 365.25, 0)  between 20 and 29 then  
										'de 20 a 29'
									when  truncate(datediff(curdate(), c.birthdate) / 365.25, 0)  > 40 then  
										'maior que 40'
									end as faixa_idade  
								, count(c.id) as total
								from pessoas c  
								group by 1;");

	      $query = $database->store_result();
	      
	      $data = array('faixa_idade' => [],'total' => []);
	     
	      while ($row = $query->fetch_assoc()) {
	      	array_push($data['faixa_idade'], $row['faixa_idade']);
	        array_push($data['total'], $row['total']);
	      }

	   	close_database($database);
	   	return($data);
	}

?>