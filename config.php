<?php		

define('DB_NAME', 'person_cad');		
define('DB_USER', 'root');		
define('DB_PASSWORD', '');	

define('DB_HOST', 'localhost');		

/** caminho para a pasta do sistema **/	
if ( !defined('ABSPATH') )		
	define('ABSPATH', dirname(__FILE__) . '/');	

/** caminho no server para o sistema **/	
if ( !defined('BASEURL') )		
	define('BASEURL', '/test-frontend-web-based/');

/** caminho do arquivo de banco de dados **/	
if ( !defined('DBAPI') )		
	define('DBAPI', ABSPATH . 'inc/database.php');	

/** caminho header e footer **/	
define('HEADER_TEMPLATE', ABSPATH . 'inc/header.php');	
define('FOOTER_TEMPLATE', ABSPATH . 'inc/footer.php');
?>