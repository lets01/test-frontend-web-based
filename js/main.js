
//Grafico de barras para faixa de idades//

var canvas ;
var context ;
var Val_Max;
var Val_Min;
var sections;
var xScale;
var yScale;
var y;

var itemName = [ "0 a 9", "10 a 19", "20 a 29", "30 a 39" , "mais que 40"]; //faixa de idades
var itemValue = [ 7, 1, 3, 1, 4 ]; // teria a quantidade de pessoas para cada faixa de idades
                                    //dados1
function init() {
    sections = 5;
    Val_Max = 14;
    var stepSize = 1;
    var columnSize = 50;
    var rowSize = 60;
    var margin = 10;
    var header = "qtd Pessoas" 
        
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    context.fillStyle = "#000;"
    
    yScale = (canvas.height - columnSize - margin) / (Val_Max);
    xScale = (canvas.width - rowSize) / (sections + 1);
    
    context.strokeStyle="#000;"; // background black lines
    context.beginPath();
        // column names 
    context.font = "19 pt Arial;"
    context.fillText(header, 0,columnSize - margin);
        // draw lines in the background
    context.font = "16 pt Helvetica"
    var count =  0;
    for (scale=Val_Max;scale>=0;scale = scale - stepSize) {
        y = columnSize + (yScale * count * stepSize); 
        context.fillText(scale, margin,y + margin);
        context.moveTo(rowSize,y)
        context.lineTo(canvas.width,y)
        count++;
    }
    context.stroke();

    context.font = "20 pt Verdana";
    context.textBaseline="bottom";
    for (i=0;i<5;i++) {
        computeHeight(itemValue[i]);
        context.fillText(itemName[i], xScale * (i+1),y - margin);
    }
  
    context.fillStyle="#9933FF;";
  context.shadowColor = 'rgba(128,128,128, 0.5)';

    context.shadowOffsetX = 9;
    context.shadowOffsetY = 3;

  context.translate(0,canvas.height - margin);
    context.scale(xScale,-1 * yScale);
  
    for (i=0;i<5;i++) {
        context.fillRect(i+1, 0, 0.3, itemValue[i]);
    }
}

function computeHeight(value) {
    y = canvas.height - value * yScale ;    
}

//responsavel pela movimentacao do sidebar
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});


//DataTable para ajudar a trabalhar com a tabela em listar pessoas
$(document).ready(function(){
    $('#table-pessoas').DataTable({
        "language": {
            "lengthMenu": "_MENU_ Pessoas por página",
            "zeroRecords": "Nada encontrado, desculpe.",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhuma pessoas disponível",
            "infoFiltered": "(filtrado de _MAX_ pessoas totais.)",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "sSearch": "Pesquisar",
        }
    });
});


//modal para excluir pessoas
$('#delete-modal').on('show.bs.modal', function (event) {         
    var button = $(event.relatedTarget);      
    var id = button.data('pessoa');       
    var modal = $(this);      
    modal.find('.modal-title').text('Excluir Pessoa #' + id);    
    modal.find('#confirm').attr('href', 'delete.php?id=' + id); 
})  


