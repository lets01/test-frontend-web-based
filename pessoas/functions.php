<?php 

require_once('../config.php');	
require_once(DBAPI);

$pessoas = null;	
$pessoas = null;

/*  Lista Clientes	 */	
function index() {		
	global $pessoas;		
	$pessoas = find_all('pessoas');	
}

/*   Adiciona Clientes	 */	
function add() {		  
	if (!empty($_POST['pessoa'])) {	 		    
		$pessoa = $_POST['pessoa'];	 
		$today = date_create('now', new DateTimeZone('America/Sao_Paulo'));   
		$pessoa['modified'] = $pessoa['created'] = $today->format("Y-m-d H:i:s");	    	    
		save('pessoas', $pessoa);
		header('location: index.php');	  		
	}	
}

function delete($id = null) {		  
	global $pessoa;	  
	$pessoa = remove('pessoas', $id);		  
	header('location: index.php');	
}

function view($id = null) {	  
	global $pessoa;	  
	$pessoa = find('pessoas', $id);	
}

/*  	Atualizacao/Edicao de Cliente	 */	
function edit() {		  
	$now = date_create('now', new DateTimeZone('America/Sao_Paulo'));		  
	if (isset($_GET['id'])) {		    
		$id = $_GET['id'];		    
		if (isset($_POST['pessoa'])) {		      
			$pessoa = $_POST['pessoa'];	      
			$pessoa['modified'] = $now->format("Y-m-d H:i:s");		      
			update('pessoas', $id, $pessoa);	      
			header('location: index.php');	    
		} else {		      
			global $pessoa;	      
			$pessoa = find('pessoas', $id);	    
		} 	  
	} else {	    
		header('location: index.php');	  
	}	
}



?>