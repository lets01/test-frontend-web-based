<?php	    
require_once('functions.php');	    
index();	
?>	
<?php include(HEADER_TEMPLATE); ?>	
		
<div class="container-fluid">
	
	<div class="row">			
		<div class="col-sm-6">				
			<h2>Pessoas</h2>			
		</div>			
		<div class="col-sm-6 text-right h2">		    	
			<a class="btn btn-primary" href="add.php">
				<i class="fa fa-plus"></i> 
				Nova Pessoa
			</a>		    	
			<a class="btn btn-default" href="index.php">
				<i class="fa fa-refresh"></i> 
				Atualizar
			</a>		    
		</div>		
	</div>	
			
	<?php if (!empty($_SESSION['message'])) : ?>		
		<div class="alert alert-<?php echo $_SESSION['type']; ?> alert-dismissible" role="alert">			
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>			
			<?php echo $_SESSION['message']; ?>		
		</div>		
		<?php clear_messages(); ?>	
	<?php endif; ?>	

	<hr>
	
	<div class="panel-body table-responsive">	
		<table class="table table-responsive" id="table-pessoas">	
			<thead>		
				<tr>			
					<th>ID</th>			
					<th>Nome</th>			
					<th>CPF/CNPJ</th>			
					<th>Atualizado em</th>			
					<th>Opções</th>		
				</tr>	
			</thead>	
			<tbody>	
				<?php if ($pessoas) : ?>	
					<?php foreach ($pessoas as $pessoa) : ?>		
						<tr>			
							<td><?php echo $pessoa['id']; ?></td>			
							<td><?php echo $pessoa['name']; ?></td>			
							<td><?php echo $pessoa['cpf_cnpj']; ?></td>		
							<td><?php echo $pessoa['modified']; ?>
							</td>			
							<td class="actions text-right">				
								<a href="view.php?id=<?php echo $pessoa['id']; ?>" class="btn btn-sm btn-success"><i class="fa fa-eye"></i> Mostrar</a>	

								<a href="edit.php?id=<?php echo $pessoa['id']; ?>" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Editar</a>	

								<a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-modal" data-pessoa="<?php echo $pessoa['id']; ?>">					
									<i class="fa fa-trash"></i> Excluir				
								</a>			
							</td>		
						</tr>	
					<?php endforeach; ?>	
					<?php else : ?>		
					<tr>			
						<td colspan="6">Nenhum registro encontrado.</td>		
					</tr>	
				<?php endif; ?>	
				</tbody>	
			</table>	
		</div>
	</div>
	
<?php include('modal.php'); ?>
<?php include(FOOTER_TEMPLATE); ?>