<?php 	  
    require_once('functions.php'); 	  
    edit();	
?>	
<?php include(HEADER_TEMPLATE); ?>	


    <h2>Editar Pessoas</h2>	

    <form action="edit.php?id=<?php echo $pessoa['id']; ?>" method="post">	  
        <hr />	  
        <div class="row">	    
            <div class="form-group col-md-7">	      
                <label for="name">Nome</label>	      
                <input type="text" class="form-control" name="pessoa['name']" value="<?php echo $pessoa['name']; ?>">
            </div>	
            <div class="form-group col-md-3">	      
                <label for="campo2">CNPJ / CPF</label>	     
                <input type="text" class="form-control" name="pessoa['cpf_cnpj']" value="<?php echo $pessoa['cpf_cnpj']; ?>">	    
            </div>	
            <div class="form-group col-md-2">	      
                <label for="campo3">Data de Nascimento</label>	      
                <input type="text" class="form-control" name="pessoa['birthdate']" value="<?php echo $pessoa['birthdate']; ?>">	    
            </div>	  
        </div>	  
        <div class="row">	    
            <div class="form-group col-md-5">	      
                <label for="campo1">Endereço</label>	      
                <input type="text" class="form-control" name="pessoa['address']" value="<?php echo $pessoa['address']; ?>">	    
            </div>	
            <div class="form-group col-md-3">         
                <label for="campo2">Sexo</label>        
                <input type="text" class="form-control" name="pessoa['gender']" value="<?php echo $pessoa['gender']; ?>">
            </div>    
        </div>	   
        <div id="actions" class="row">	    
            <div class="col-md-12">	      
                <button type="submit" class="btn btn-primary">Salvar</button>	      
                <a href="index.php" class="btn btn-default">Cancelar</a>	   
            </div>	  
        </div>	
    </form>	
    
<?php include(FOOTER_TEMPLATE); ?>