<?php 	  
	require_once('functions.php'); 	  
	add();	
?>	

<?php include(HEADER_TEMPLATE); ?>	

<div class="container-fluid">

	<h2>Adicionar Pessoas</h2>
	<form action="add.php" method="post">	 
		<hr/>  
		<div class="row">	
			<div class="form-group col-md-8">	      
				<label for="name">Nome</label>	      
				<input type="text"  class="form-control" name="pessoa['name']" required="true">	    
			</div>	
			<!-- não consegui colocar as mascaras a tempo... -->
		    <div class="form-group col-md-4">	      
		    	<label for="campo2">CNPJ/CPF</label>	      
		    	<input type="text"  class="form-control" name="pessoa['cpf_cnpj']" required="true">	    
		    </div>			    	  
		</div>	  	  
		<div class="row">
			<!-- não consegui colocar as mascaras a tempo... -->	    
			<div class="form-group col-md-3">	      
		    	<label for="campo3">Data de Nascimento</label>	      
		    	<input type=""  class="form-control" name="pessoa['birthdate']" required="true">	    
		    </div>
			<div class="form-group col-md-6">	      
				<label for="campo1">Endereço</label>	      
				<input type="text"  class="form-control" name="pessoa['address']" required="true">	    
			</div>
			<!-- poderia ser uma selecao, mas não consegui terminar... -->	
		    <div class="form-group col-md-3">	      
		    	<label for="campo2">Sexo</label>	      
		    	<input type="text"  class="form-control" name="pessoa['gender']" required="true">	    
		    </div>	    	    
		</div>	  	 
		<div id="actions" class="row">	    
			<div class="col-md-12">	      
				<button type="submit" class="btn btn-primary">Salvar</button>	      
				<a href="index.php" class="btn btn-default">Cancelar</a>	    
			</div>	  
		</div>		
	</form>

</div>

<?php include(FOOTER_TEMPLATE); ?>