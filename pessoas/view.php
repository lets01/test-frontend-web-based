<?php 
	require_once('functions.php');  
	view($_GET['id']);	
?>

<?php include(HEADER_TEMPLATE);?>

	<!-- 
		Aqui os dados da tupla escolhida no banco de dados 
		são mostrados para o usuário...  
	-->	

	<h2><?php echo $pessoa['name']; ?></h2>	
	<hr>

	<?php if (!empty($_SESSION['message'])) : ?>		
		<div class="alert alert-<?php echo $_SESSION['type']; ?>"><?php echo $_SESSION['message']; ?></div>	
	<?php endif; ?>

	<dl class="dl-horizontal">		
		<dt>CPF / CNPJ:</dt>		
		<dd><?php echo $pessoa['cpf_cnpj']; ?></dd>	
		<dt>Data de Nascimento:</dt>		
		<dd><?php echo $pessoa['birthdate']; ?></dd>		
		<dt>Endereço:</dt>		
		<dd><?php echo $pessoa['address']; ?></dd>	
		<dt>sexo:</dt>		
		<dd><?php echo $pessoa['gender']; ?></dd>	
	</dl>	

	<dl class="dl-horizontal">		
		<dt>Criado em:</dt>		
		<dd><?php echo $pessoa['created']; ?></dd>	
		<dt>modificado em:</dt>		
		<dd><?php echo $pessoa['modified']; ?></dd>	
	</dl>
		
	<div id="actions" class="row">		
		<div class="col-md-12">		  
			<a href="edit.php?id=<?php echo $pessoa['id']; ?>" class="btn btn-primary">Editar</a>		  
			<a href="index.php" class="btn btn-default">Voltar</a>		
		</div>	
	</div>	
	
<?php include(FOOTER_TEMPLATE); ?>