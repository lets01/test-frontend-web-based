CREATE TABLE IF NOT EXISTS `pessoas` (
 `id`  INT NOT NULL,
 `name`  VARCHAR(255) NOT NULL,
  `birthdate` DATE NOT NULL,
  `cpf_cnpj` INT NOT NULL,
  `gender` char NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `created` DATETIME, -- teste
  `modified` DATETIME -- teste
)ENGINE = InnoDB;

ALTER TABLE `pessoas`	  
ADD PRIMARY KEY (id);
	  
ALTER TABLE `pessoas`	  
MODIFY id int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

